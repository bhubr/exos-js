// Recoder la fonction substr de JS

const assert = require('assert');

const substr = (string, indexStart, length) => {
  let result = '';
  for (let i = indexStart ; i < string.length && (!length || result.length < length) ; i++) {
    result += string[i];
  }
  return result;
}

assert.strictEqual('is cool', substr('Coding is cool', 7));
assert.strictEqual('is', substr('Coding is cool', 7, 2));