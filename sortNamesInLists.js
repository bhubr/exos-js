// à partir d'un tableau d'objets contenant des noms,
// créer un tableau à deux dimensions. Chacun des sous-tableaux
// doit contenir les noms commençant par une certaine lettre.

const assert = require('assert');

const sortNamesInLists = namesArray => {
  const result = [];
  const indicesByLetter = {};
  for(let name of namesArray) {
    const firstLetter = name[0].toLowerCase();
    if(! (firstLetter in indicesByLetter)) {
      indicesByLetter[firstLetter] = result.length;
      result.push([name]);
    }
    else {
      const index = indicesByLetter[firstLetter];
      result[index].push(name);
    }
  }
  return result;
}

assert.deepStrictEqual([
  ['George Harrison', 'Gregory Porter'],
  ['Coldplay'],
  ['Jack Johnson', 'Jimi Hendrix', 'John Mayer', 'Joan Baez'],
  ['Suzanne Vega', 'Sting', 'Sheryl Crow'],
  ['Eva Cassidy', 'Eagles'],
  ['Laura Marling']
], sortNamesInLists([
  'George Harrison', 'Coldplay', 'Jack Johnson', 'Jimi Hendrix', 'Gregory Porter', 'Suzanne Vega',
  'Eva Cassidy', 'John Mayer', 'Sting', 'Laura Marling', 'Joan Baez', 'Sheryl Crow', 'Eagles'
]));
