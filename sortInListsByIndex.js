
// Construire un tableau à 2d à partir d'un tableau 1d,
// en plaçant dans chacun les éléments en fonction de leur index pair ou impair
// d'origine dans le tableau
const assert = require('assert');

const sortInListsByIndex = array => {
  const result = [[], []];
  for (let i = 0 ; i < array.length ; i++) {
    result[i % 2].push(array[i]);
  }
  return result;
}

assert.deepStrictEqual([
  ["C'est", 'moins', 'comme'],
  ['beaucoup', 'lisible', 'ça !']
], sortInListsByIndex([
  "C'est", 'beaucoup', 'moins', 'lisible', 'comme', 'ça !'
]));
