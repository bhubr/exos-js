// à partir d'un mot construire un objet
const assert = require('assert');

const charFirstFoundAt = str => {
  const result = {};
  for (let i = 0 ; i < str.length ; i++) {
    const char = str[i];
    if (result[char] === undefined) {
      result[char] = i;
    }
  }
  return result;
}

assert.deepStrictEqual({
  c: 0,
  h: 1,
  o: 2,
  l: 5,
  a: 6,
  t: 7
}, charFirstFoundAt('chocolat'));