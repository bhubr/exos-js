// Eliminer toutes les occurences d'un caractère dans une chaîne

const assert = require('assert');

const removeCharFromString = (string, char) => {
  let result = '';
  for (let i = 0 ; i < string.length ; i++) {
    if (string[i] !== char) {
      result += string[i];
    }
  }
  return result;
}

assert.strictEqual(
  'This message was seriously messed up',
  removeCharFromString('Tzhziszz zzzmeszsazgze zwzasz szezrizozuslzy mzeszszzezzdz zzuzp', 'z')
);