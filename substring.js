// Recoder la fonction substring de JS

const assert = require('assert');

const substring = (string, indexStart, indexEnd) => {
  let result = '';
  for (let i = indexStart ; i < string.length && (!indexEnd || i < indexEnd) ; i++) {
    result += string[i];
  }
  return result;
}

assert.strictEqual('Script', substring('JavaScript', 4));
assert.strictEqual('Scri', substring('JavaScript', 4, 8));