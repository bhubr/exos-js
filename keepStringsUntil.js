// Garder les chaînes d'un tableau jusqu'à trouver une avec un certain nom
// Deux façons:
// - return si trouve avec un nom,
// - indexOf puis slice

const assert = require('assert');

const keepStringsUntil = (wordsArray, stopWord) => {
  const result = [];
  let i = 0;
  while (i < wordsArray.length && wordsArray[i] !== stopWord) {
    result.push(wordsArray[i]);
    i++;
  }
  return result;
};

assert.deepStrictEqual(
  ['I', 'should', 'not', 'go', 'beyond'],
  keepStringsUntil(['I', 'should', 'not', 'go', 'beyond', 'that', 'word'], 'that')
);