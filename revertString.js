// inverser les caractères d'une string

const assert = require('assert');

const revertString = str => {
  let output = '';
  for(let i = str.length - 1 ; i >= 0 ; i--) {
    output += str[i];
  }
  return output;
}

assert.strictEqual('JavaScript rocks!', revertString('!skcor tpircSavaJ'));