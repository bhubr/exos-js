// Ne garder qu'une occurence d'un élément dans un tableau
// (de chaînes ou nombres). Inspiré (copié même) de lodash

const assert = require('assert');

const uniq = array => {
  let result = [];
  for (let i = 0 ; i < array.length ; i++) {
    if (!result.includes(array[i])) {
      result.push(array[i]);
    }
  }
  return result;
}

assert.deepStrictEqual(
  [1, 8, 5, 7, 3, 2, 4],
  uniq([1, 8, 5, 7, 3, 8, 2, 1, 5, 4])
);
