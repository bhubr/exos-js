// Renvoyer une sous-chaîne en s'arrêtant quand on rencontre le caractère spécifié en 2nd
const assert = require('assert');

// const substrUntilChar = (string, char) => {
//   let result = '';
//   for (let i = 0 ; i < string.length && string[i] !== char ; i++) {
//     result += string[i];
//   }
//   return result;
// }

const substrUntilChar = (string, char) => {
  let result = '';
  for (let i = 0 ; i < string.length; i++) {
    if (string[i] === char) {
      return result;
    }
    result += string[i];
  }
  return result;
}

assert.strictEqual("Don't stop until ", substrUntilChar("Don't stop until you're done!", 'y'));