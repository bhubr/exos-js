// intervertir paires de lettre dans une string!

const assert = require('assert');

const swapLetters = str => {
  let output = '';
  for (let i = 0 ; str[i] ; i += 2) {
    if (str[i + 1] === undefined) {
      output += str[i];
      return output;
    }
    output += str[i + 1];
    output += str[i];
  }
  return output;
};

assert.strictEqual('Quel exercice tordu !', swapLetters('uQlee excrci eotdr u!'));
assert.strictEqual('', swapLetters(''));
assert.strictEqual('?', swapLetters('?'));
assert.strictEqual('!?', swapLetters('?!'));