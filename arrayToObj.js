// à partir d'un tableau construire un objet
const assert = require('assert');

const arrayToObj = array => {
  const object = {};
  for (let element of array) {
    object[element.key] = element.value;
  }
  return object;
}

// const arrayToObj = array => array.reduce(
//   (carry, { key, value }) => ({ ...carry, [key]: value }), {}
// )

assert.deepStrictEqual({
  firstName: 'Homer',
  lastName: 'Simpson',
  city: 'Springfield',
  job: 'Nuclear Safety Inspector'
}, arrayToObj([
  { key: 'firstName', value: 'Homer' },
  { key: 'lastName', value: 'Simpson' },
  { key: 'city', value: 'Springfield' },
  { key: 'job', value: 'Nuclear Safety Inspector' }
]));