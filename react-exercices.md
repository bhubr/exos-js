# Exercices React

* Convertisseur de devises
- Simple: euros - dollar
- Ajout de choix de de devise sur chaque champ

* Formulaire d'inscription
- Validation des champs (nom doit contenir caractères européens)

* Onglets

* Cacher / Montrer valeur
* Cacher / Montrer composant
* BARRE DE PROGRESSION
* Faire bouger qq chose en position absolute
* Choisir entre deux composants: sur *un seul*, sur *plusieurs* (l'idée que j'avais pour le filter)
* Afficher / Cacher mot de passe
* Générer mot de passe aléatoire
* Fonctions qui renvoient du JSX
* Ajouter une valeur au state / la supprimer
* Todo list
* Mesurer force mot de passe
  - expressions régulières
  - algo tutsplus: https://code.tutsplus.com/tutorials/build-a-simple-password-strength-checker--net-7565
  - lib dropbox: https://github.com/dropbox/zxcvbn trouvée depuis https://nulab-inc.com/blog/nulab/password-strength/