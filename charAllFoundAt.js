// à partir d'un mot construire un objet
const assert = require('assert');

const charAllFoundAt = str => {
  const result = {};
  for (let i = 0 ; i < str.length ; i++) {
    const char = str[i];
    if (result[char] === undefined) {
      result[char] = [i];
    }
    else {
      result[char].push(i);
    }
  }
  return result;
}

assert.deepStrictEqual({
  c: [0, 3],
  h: [1],
  o: [2, 4],
  l: [5],
  a: [6],
  t: [7]
}, charAllFoundAt('chocolat'));

assert.deepStrictEqual({
  a: [0, 3, 5, 7, 10],
  b: [1, 8],
  r: [2, 9],
  c: [4],
  d: [6]
}, charAllFoundAt('abracadabra'));